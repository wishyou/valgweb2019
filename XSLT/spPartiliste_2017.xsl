<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <TABLE BORDER="1" BGCOLOR="#ffffff" CELLSPACING="0" CELLPADDING="3">
      <xsl:call-template name="tablehead"/>
      <xsl:apply-templates select="partiliste/parti"/>
    </TABLE>
  </xsl:template>

  <xsl:template match="partiliste/parti">
    <tr>
      <td>
        <xsl:value-of select="kode"/>
      </td>
      <td>
        <xsl:value-of select="navn"/>
      </td>
	<td>
		<xsl:choose>
			<xsl:when test="kategori='1'">Offisiellt parti</xsl:when>
			<xsl:when test="kategori='2'">Registrert parti</xsl:when>
      <xsl:when test="kategori='3'">Liste</xsl:when>
      <xsl:otherwise>-&#160;</xsl:otherwise>
		</xsl:choose></td>

    </tr>
  </xsl:template>

  <xsl:template name="tablehead">
    <TR>
      <TH style="width: 2cm">
        <xsl:text>Partikode</xsl:text>
      </TH>
      <TH style="width: 12cm">
        <xsl:text>Partinavn</xsl:text>
      </TH>
	<TH style="width: 3cm">
	<xsl:text>Partitype</xsl:text>
	</TH>

    </TR>
  </xsl:template>

  <xsl:template match="qPartiliste">

  </xsl:template>

</xsl:stylesheet>