<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="no"/>

  <xsl:param name="FNr"></xsl:param>
  <xsl:param name="ByNr"></xsl:param>
  <xsl:param name="valgtype">K</xsl:param>

  <xsl:variable name="Form">Default.aspx</xsl:variable>

  <xsl:key name="kretsitems_by_kommnr" match="rapport" use="data[@navn='KommNr']" />
  
  <!-- Regionreform 2019 election -->
  <xsl:variable name="count_11" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '11' and 
  (data[@navn='AntKretserFhstOpptalt'] > 0 or data[@navn='AntKretserVtstOpptalt'] > 0 or data[@navn='AntKretserAltOpptalt'] > 0) ])" />
  <xsl:variable name="max_11" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '11'])"/>
  
  <xsl:variable name="count_15" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '15' and 
  (data[@navn='AntKretserFhstOpptalt'] > 0 or data[@navn='AntKretserVtstOpptalt'] > 0 or data[@navn='AntKretserAltOpptalt'] > 0) ])" />
  <xsl:variable name="max_15" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '15'])"/>

  <xsl:variable name="count_18" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '18' and
  (data[@navn='AntKretserFhstOpptalt'] > 0 or data[@navn='AntKretserVtstOpptalt'] > 0 or data[@navn='AntKretserAltOpptalt'] > 0) ])" />
  <xsl:variable name="max_18" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '18'])"/>

  <xsl:variable name="count_30" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '30' and 
  (data[@navn='AntKretserFhstOpptalt'] > 0 or data[@navn='AntKretserVtstOpptalt'] > 0 or data[@navn='AntKretserAltOpptalt'] > 0) ])" />
  <xsl:variable name="max_30" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '30'])"/>

  <xsl:variable name="count_34" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '34' and 
  (data[@navn='AntKretserFhstOpptalt'] > 0 or data[@navn='AntKretserVtstOpptalt'] > 0 or data[@navn='AntKretserAltOpptalt'] > 0) ])" />
  <xsl:variable name="max_34" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '34'])"/>

  <xsl:variable name="count_38" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '38' and 
  (data[@navn='AntKretserFhstOpptalt'] > 0 or data[@navn='AntKretserVtstOpptalt'] > 0 or data[@navn='AntKretserAltOpptalt'] > 0) ])" />
  <xsl:variable name="max_38" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '38'])"/>

  <xsl:variable name="count_42" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '42' and 
  (data[@navn='AntKretserFhstOpptalt'] > 0 or data[@navn='AntKretserVtstOpptalt'] > 0 or data[@navn='AntKretserAltOpptalt'] > 0) ])" />
  <xsl:variable name="max_42" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '42'])"/>

  <xsl:variable name="count_46" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '46' and 
  (data[@navn='AntKretserFhstOpptalt'] > 0 or data[@navn='AntKretserVtstOpptalt'] > 0 or data[@navn='AntKretserAltOpptalt'] > 0) ])" />
  <xsl:variable name="max_46" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '46'])"/>

  <xsl:variable name="count_50" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '50' and 
  (data[@navn='AntKretserFhstOpptalt'] > 0 or data[@navn='AntKretserVtstOpptalt'] > 0 or data[@navn='AntKretserAltOpptalt'] > 0) ])" />
  <xsl:variable name="max_50" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '50'])"/>

  <xsl:variable name="count_54" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '54' and 
  (data[@navn='AntKretserFhstOpptalt'] > 0 or data[@navn='AntKretserVtstOpptalt'] > 0 or data[@navn='AntKretserAltOpptalt'] > 0) ])" />
  <xsl:variable name="max_54" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '54'])"/>

  <xsl:template match="respons">
    <table width="100%" border="0" cellpadding="1" cellspacing="0">

    <xsl:choose>
		<xsl:when test="$valgtype='F'">
			<xsl:call-template name="oslo"/>
		</xsl:when>
		<xsl:otherwise>
      <xsl:apply-templates select="/respons/rapport[data[@navn='KommNr']='0301']" mode="by">
        <xsl:with-param name="FBNr" select="'03'"/>
      </xsl:apply-templates>
    </xsl:otherwise>
	</xsl:choose>

      <!--<xsl:apply-templates select="/respons/rapport[data[@navn='KommNr']='0301']" mode="by">
        <xsl:with-param name="FBNr" select="'03'"/>
      </xsl:apply-templates>-->

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'11' != $FNr">
                <xsl:text>11</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Rogaland</xsl:text>
            </xsl:attribute>
            <xsl:text>Rogaland</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_11"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_11"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_11"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_11"/>
          </abbr>
        </td>
      </tr>
      <xsl:if test="$FNr='11'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'15' != $FNr">
                <xsl:text>15</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Møre og Romsdal</xsl:text>
            </xsl:attribute>
            <xsl:text>Møre og Romsdal</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_15"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_15"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_15"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_15"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='15'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'18' != $FNr">
                <xsl:text>18</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Nordland</xsl:text>
            </xsl:attribute>
            <xsl:text>Nordland</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_18"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_18"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_18"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_18"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='18'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'30' != $FNr">
                <xsl:text>30</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Viken</xsl:text>
            </xsl:attribute>
            <xsl:text>Viken</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_30"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_30"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_30"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_30"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='30'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'34' != $FNr">
                <xsl:text>34</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Innlandet</xsl:text>
            </xsl:attribute>
            <xsl:text>Innlandet</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_34"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_34"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_34"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_34"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='34'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'38' != $FNr">
                <xsl:text>38</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Vestfold og Telemark</xsl:text>
            </xsl:attribute>
            <xsl:text>Vestfold og Telemark</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_38"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_38"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_38"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_38"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='38'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'42' != $FNr">
                <xsl:text>42</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Agder</xsl:text>
            </xsl:attribute>
            <xsl:text>Agder</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_42"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_42"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_42"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_42"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='42'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'46' != $FNr">
                <xsl:text>46</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Vestland</xsl:text>
            </xsl:attribute>
            <xsl:text>Vestland</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_46"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_46"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_46"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_46"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='46'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'50' != $FNr">
                <xsl:text>50</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Trøndelag</xsl:text>
            </xsl:attribute>
            <xsl:text>Trøndelag</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_50"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_50"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_50"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_50"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='50'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'54' != $FNr">
                <xsl:text>54</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Troms og Finnmark</xsl:text>
            </xsl:attribute>
            <xsl:text>Troms og Finnmark</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_54"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_54"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_54"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_54"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='54'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

    </table>
    <p/>
    <table width="100%" cellpadding="1" cellspacing="0" border="0">
      <tr>
        <td class='status' colspan="2" valign='top'>
          <b>Tallene på fylkesnivå viser:</b><br/>
        Antall kommuner som har rapportert kresultater/totalt antall kretsrapporterende kommuner<br/>&#160;
      </td>
      </tr>
      <tr>
        <td class='status' colspan="2" valign='top'>
          <b>Tallene på kommunenivå viser:</b><br/>
        Antall kretser som har rapportert resultater/totalt antall kretser i kommunen</td>
      </tr>
    </table>

  </xsl:template>

  <xsl:template name="oslo">
    <tr>
      <td colspan="2">
        <a>
          <xsl:attribute name="href">
            <xsl:value-of select="$Form"/>
            <xsl:text>?liste=oslo</xsl:text>
            <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
          </xsl:attribute>Oslo bystyreoversikt
        </a>
      </td>
      <td class="status" align="right">104</td>

    </tr>
  </xsl:template>

  <xsl:template match="/respons/rapport" mode="by">
    <xsl:param name="FBNr" />
    <xsl:variable name="KommNr">
      <xsl:value-of select="data[@navn='KommNr']"/>
    </xsl:variable>
    <xsl:variable name="KommNavn">
      <xsl:value-of select="data[@navn='KommNavn']"/>
    </xsl:variable>

    <xsl:if test ="count(. | key('kretsitems_by_kommnr', $KommNr)[1]) = 1">

      <tr>
        <xsl:if test="$FBNr != '03'">
          <td width="5"></td>
        </xsl:if>

        <td>
          <xsl:if test="$FBNr = '03'">
            <xsl:attribute name="colspan">2</xsl:attribute>
          </xsl:if>
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?ByNr=</xsl:text>
              <xsl:if test="$KommNr != $ByNr">
                <xsl:value-of select="$KommNr"/>
              </xsl:if>
              <xsl:text>&amp;FByNr=</xsl:text>
              <xsl:value-of select="$FBNr"/>
              <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
              <xsl:text>&amp;KommuneNr=</xsl:text>
              <xsl:value-of select="$KommNr"/>

              <xsl:text>&amp;Navn=</xsl:text>
              <xsl:value-of select="$KommNavn"/>
            </xsl:attribute>
            <xsl:value-of select="$KommNavn"/>
          </a>
        </td>

        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Rapporter mottatt for </xsl:text>
              <xsl:value-of select="count(key('kretsitems_by_kommnr', $KommNr)[data[@navn='StatusInd'] != 0])"/>
              <xsl:text> av </xsl:text>
              <xsl:value-of select="data[@navn='TotAntKretser']"/>
              <xsl:text> kretser</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="count(key('kretsitems_by_kommnr', $KommNr)[data[@navn='StatusInd'] != 0])"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="data[@navn='TotAntKretser']"/>
          </abbr>
        </td>
      </tr>
      <xsl:if test="$KommNr=$ByNr">
        <tr>
          <td width="5"></td>
          <td align="left">
            <a>
              <xsl:attribute name="href">
                <xsl:value-of select="$Form"/>
                <xsl:text>?ByNr=</xsl:text>
                <xsl:value-of select="$KommNr"/>
                <xsl:text>&amp;FByNr=</xsl:text>
                <xsl:value-of select="$FBNr"/>
                <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
                <xsl:text>&amp;KommuneNr=</xsl:text>
                <xsl:value-of select="$KommNr"/>
                <xsl:text>&amp;AlleKretser=1</xsl:text>
                <xsl:text>&amp;Navn=</xsl:text>
                <xsl:value-of select="$KommNavn"/>
              </xsl:attribute>
              - Alle
            </a>
          </td>
          <td width="20"></td>

        </tr>
      </xsl:if>
      <xsl:if test="$KommNr=$ByNr">
        <tr>
          <td width="5"></td>
          <td colspan="2">
            <table width="100%"  border="0" cellpadding="1" cellspacing="0">
              <xsl:apply-templates select="/respons/rapport[data[@navn='KommNr'] = $KommNr and data[@navn='KretsNr'] != '0000' and data[@navn='AntFrammotte'] != 0]" mode="krets">
                <xsl:with-param name="FBNr" select="$FNr"/>
                <xsl:sort select="data[@navn='KretsNavn']"></xsl:sort>
              </xsl:apply-templates>
              <xsl:apply-templates select="/respons/rapport[data[@navn='KommNr'] = $KommNr and data[@navn='KretsNr'] = '0000' and data[@navn='AntFrammotte'] != 0]" mode="krets">
                <xsl:with-param name="FBNr" select="$FNr"/>
              </xsl:apply-templates>
            </table>
          </td>
        </tr>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template match="*">
  </xsl:template>

  <xsl:template match="/respons/rapport" mode="krets">
    <xsl:param name="FBNr" />
    <tr>
      <td width="5"></td>
      <td>
        <a>
          <xsl:attribute name="href">
            <xsl:value-of select="$Form"/>
            <xsl:text>?ByNr=</xsl:text>
            <xsl:value-of select="$ByNr"/>
            <xsl:text>&amp;FByNr=</xsl:text>
            <xsl:value-of select="$FBNr"/>
            <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
            <xsl:text>&amp;KretsNr=</xsl:text>
            <xsl:value-of select="data[@navn='KretsNr']"/>
            <xsl:text>&amp;Navn=</xsl:text>
            <xsl:value-of select="data[@navn='KretsNavn']"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="data[@navn='KretsNr'] = 'VIRT'">
              <xsl:text>Forhåndsstemmer</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="data[@navn='KretsNavn']"/>
            </xsl:otherwise>
          </xsl:choose>
        </a>
      </td>
      <!--
	<td class="status">
			(<xsl:value-of select="data[@navn='StatusInd']"/>)
	</td>
	-->
    </tr>
  </xsl:template>


</xsl:stylesheet>